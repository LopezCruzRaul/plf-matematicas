# Conjuntos, Aplicaciones y funciones
Fuente: 
- [Conjuntos, Aplicaciones y funciones (2002)]

```plantuml
@startmindmap
* Conjuntos, Aplicaciones y Funciones
 * Conjuntos
  *_ ¿Qué es?
   * Una colección de elementos
   * Elementos con relaciones agrupados
   * Existe relación de pertenencia
  *_ Tipos
   * Universal
    * Contiene a todos los elementos
   * Vacío
    * No continene ningún elemento
  *_ Operaciones
   * Intersección
    *_ es
     * Elementos que pertenecen al mismo \ntiempo a dos o más conjuntos
      *_ Ejemplo
       * AnB = Todos los elementos \nque pertenezcan a A **y** B al \nmismo tiempo
   * Union
    * Elementos que pertenecen a al menos uno de los conjuntos
     *_ Ejemplo
      * AuB = Todos los elementos \nque pertenecen a A **o** B
   * Complementación
    * Elementos que NO pertenecen al conjunto
     *_ Ejemplo
      * Ac = Todos los elementos que están \nfuera del conjunto A
   * Diferencia
    * Elementos que pertenecen al conjunto pero no a otro conjunto definido
     *_ Ejemplo
      * A-B = Todos los elementos \nque Pertenecen a A **y** \nno forman parte de B
  * Usos
   * Ayudan a entender problemas de agrupaciones
   * Representar grupos y pertenencias de forma gráfica
   * Ayudan a comprender intuitivamente la posición de operaciones
  * Representación gráfica
   *_ Mediante dibujos denominados
    * Diagramas de Venn
 * Aplicaciones
  *_ ¿Qué es?
   * una transformación de los elementos en un conjunto
  *_ Tipos de aplicaciones
   * Aplicación inyectiva
    *_ es
     * Si dos elementos tienen la misma imagen, son el mismo elemento
   * Aplicación sobreyectiva
    *_ es
     * Cuando son equivalentes
   * Aplicación biyectiva
    *_ es
     * Cuando son Sobreyectivas e Inyectivas
 * Funciones
  *_ ¿Qué es?
   * Es una terminología
   * Son aplicaciones que se aplican sobre un conjunto de números
   * Es una aplicación que transforma un conjunto
  *_ ¿Para qué sirve?
   * Resolver problemas a través de las matemáticas
   * para comprender el entorno
   * permite conocer las funciones y su comportamiento sobre datos
  *_ como
   * Sistemas de coordenadas
    *_ son
     * Gráficas de la función
      *_ pueden ser de tipo
       * Parábolicas
       * Rectas
       * Series de puntos
  * Comportamiento
   *_ al modificar variables
    * Incremento en X e Y
    * Incremento en X, Decremento en Y
@endmindmap
```

# Funciones
Fuente: 
- [Funciones (2010)]
        
```plantuml
@startmindmap
* Funciones
 *_ ¿Qué es?
  * Análisis matemático
   *_ como
    * Aplicación especial
    * Conjunto numérico
  * Reflejo del pensamiento
   *_ con reglas para
    * Comprender el entorno
    * Resolver problemas
  * La transformación de un conjunto en otro
 *_ Comportamientos
  * Límites
   *_ es
    * Aproximación pero sin alcanzar el valor
  * Máximos
   *_ es
    * Cuando la función alcanza el valor máximo
  * Mínimos
   *_ es
    * Cuando la función alcanza el valor mínimo
  *_ Según la continuidad
   * Continua
    *_ cuyas características son
     * Funciones manejables
     * Funciones con buen comportamiento
     * Funciones con resultados más predecibles
     * No existen saltos o vacíos en la trama
   * Discontinua
    *_ con caracteristicas
     * presenta saltos o cortes
     * Funciones más complejas
     * Presenta un comportamiento predecible
      * Saltos no predecibles a simple vista
  * Dominio
   * Es un conjunto de elementos de una variable
  * Intervalos
   * Es un rango de valores dentro de un conjunto de reales o imagenes
 *_ Usos
  * Derivadas
   *_ ¿para qué sirve?
    * Aproximar una función complicada a una simple
    * Resolver el problema de una función compleja con una función lineal
   *_ Se aplica sobre
    * Productos
    * Exponentes
  * Representación cartesiana
   *_ inventada por
    * René Descartes
   *_ es
    * Representación en un plano con ejes
     * Eje 1: Valores de un conjunto de números reales
     * Eje 2: Las imágenes (resultados)
 *_ Tipos
  * Según el comportamiento
   *_ al modificar variables
    * Creciente
     * Incremento en ambos ejes
    * Decreciente
     * Incremento en el eje de números reales, decremento en el eje de imágenes
@endmindmap
```

# La matemática del computador
Fuentes:
- [La matemática del computador. El examen final (2002)]
- [Las matemáticas de los computadores. El examen final (2000)]

```plantuml
@startmindmap
* La matemática del computador
 * Matemáticas
  *_ ¿Qué es?
   * Es una herramienta indispensable para el desarrollo de los computadores
   * Una ciencia
    *_ de
     * Deducción lógica
    *_ que estudia
     * Los valores abstractos (números)
  * Aritmética del computador
   *_ es la
    * Aproximación a un número
     *_ por ejemplo
      * Pi
      * Imaginarios sqrt(2)
      * Números con punto flotante
 * Computador
  *_ Asigna valores
   * Con pasos de corriente
    *_ determina
     * Valores
     * Posiciones
     * Puntos flotantes
     * Negatividad
  * No puede representar \nnúmeros con cifras infinitas
   *_ Para ello cuenta con
    * Técnicas
     * Truncamiento
      *_ es
       * Recortar los \ndígitos de un número
        *_ ejemplo
         * 95.987 -> 95.98 \ntruncando a 4 dígitos
     * Redondeo
      *_ es
       * Aproximar un valor \na uno con menor número de \ndecimales
        *_ puede ser
         * Hacia abajo
          * Si el último dígito es \nmenor a 5, los otros dígitos \nse dejan intactos
           *_ ejemplo
            * 95.123 -> 95.12 \nredondeando a dos decimales
         * Hacia arriba
          * Si el último dígito es \n5 o mayor, el penúltimo dígito \naumenta en 1
           *_ ejemplo
            * 95.987 -> 95.98 \nredondeando a dos decimales
  * Sistemas de numeración
   * Sistema Binario
    * Base 2
    * Es el sistema más simple
    * Es utilizado por los ordenadores
    * Operaciones como
     * Adición binaria
    *_ se puede utilizar para
     * Algebra booleana
      * Dos valores únicos: True y False
    * Es posible digitalizar
     * Imágenes
     * Videos
     * Gran cantidad de multimedia
   * Sistema Octal
    * Base 8
   * Sistema Hexadecimal
    * Base 16
    * Utiliza números y letras
    * Puede representar cantidades más grandes con menos dígitos
   * Aritmética en punto flotante
   * Desbordamiento
   * Conversión Base
    * Es la técnica para convertir un número de una base a otra
     *_ ejemplo
      * Binario: 1101 \nOctal = 15 \nDecimal = 13 \nHexadecimal = D
@endmindmap
```


[La matemática del computador. El examen final (2002)]: https://canal.uned.es/video/5a6f1b76b1111f15098b45fa
[Las matemáticas de los computadores. El examen final (2000)]: https://canal.uned.es/video/5a6f1b81b1111f15098b4640
[Funciones (2010)]: https://canal.uned.es/video/5a6f2d09b1111f21778b457f
[Conjuntos, Aplicaciones y funciones (2002)]: https://canal.uned.es/video/5a6f1b77b1111f15098b4609